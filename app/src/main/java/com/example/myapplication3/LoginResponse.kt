package com.example.myapplication3

import com.google.gson.annotations.SerializedName

data class LoginResponse(@SerializedName("success") var success:Boolean)