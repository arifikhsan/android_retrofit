package com.example.myapplication3

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_login.setOnClickListener{
            RetrofitManager.createService(NetworkApi::class.java)
                    .login(LoginRequest(et_username.text.toString(), et_password.text.toString()))
                    .enqueue(object:Callback<LoginResponse>{
                        override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                            Toast.makeText(this@MainActivity,response!!.body()?.success.toString(),Toast.LENGTH_LONG).show()
                        }

                        override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                        }

                    })
        }
    }
}
