package com.example.myapplication3

import com.google.gson.annotations.SerializedName

data class LoginRequest(@SerializedName("id_pengguna") var idPengguna:String,
                        @SerializedName("katasandi") var katasandi:String)