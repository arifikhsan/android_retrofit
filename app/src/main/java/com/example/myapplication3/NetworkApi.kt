package com.example.myapplication3

import retrofit2.http.POST
import retrofit2.Call
import retrofit2.http.Body

interface NetworkApi{
    @POST("pengguna/login")
    fun login(@Body loginRequest: LoginRequest):Call<LoginResponse>
}